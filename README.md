# CANCELLED

So I've found that the Twitch (formerly Curse.com) game manager will actually sync
both your addons as well as your settings.  As far as I'm concerned, mission accomplished.
I have no desire to re-invent the wheel.  If you feel strongly otherwise, please drop
me a line (grimmtooth@gmail.com) and I may reconsider depending on the number of requests 
versus the profanity level of said requests.  But for now the project is dead.

# W O W M O N G E R

WoWMonger is a small program with two primary purposes.

1. It will back up your WoW addons and settings.
2. It will give you the means to sync your addons and settings between computers. 

There's more to it than that, but those are the basics.

_Note: the Trello integration for this project doesn't yet work in FireFox, so while I set up a board it isn't
being used. If this notice disappears, that will have changed._

**Be very aware that this is currently a work in progress. This note will disappear when it is released.**

---

## Requirements

* World of Warcraft installed on any machine you are using this on.
* Windows operating system (Windows 10 and later supported. Others may work, but 
it'll only be coincidenal if they do.)
* A free account on bitbucket.org.

That last one is the heart and soul of how this thing even works. Bitbucket.org is 
a free online source code repository. We will be using it to provide you with (1)
a place for your stuff (2) without having to cover the costs of storing your stuff
for you and (3) keeping it private to you and you only. That's it.

---

# Frequently Asked Questions

_Why don't you support GitHub as well?_

* GitHub has done some morally (to me) sketchy things in the past and I don't feel
like giving them the traffic as a result. Maybe BitBucket has issues as well, but
to date I have not heard of any instances of sexual harrasment and discrimination. 
Now that they've IPO'd that might change. 

_Why don't you support using DropBox / Google Drive / $SharingApp as well?_

* I may in the future do so.  If it matters to you, submit a feature request
ticket or upvote an existing ticket.
