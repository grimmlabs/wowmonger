# !/usr/bin/python3
#
# wowMonger! - Created 2/24/2018
#
# Monitor prices of items and alert when there are interesting prices.
#
# (c) 2018 @ Grimmlabs
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################

# todo: set up sequence to handle first time startup

import json
import sys

import wx

sys.path.append('lib')

from lib import config as cfg
from lib import version

from lib import defaultPrefs

from lib.ui import icon
from lib.ui import MainFrame
from lib.ui import appConfig


class mongerApp(wx.App):
    def OnInit(self):
        self.SetAppName(version.name)
        self.SetVendorName(version.vendor)

        # Necessary because we won't always have frames open.
        self.SetExitOnFrameDelete(False)

        # Initializing the image handlers allows the program to
        # use images in far more formats than just bitmaps.
        wx.InitAllImageHandlers()

        # load system config
        self.prefs = self.loadConfig()

        self.Tray = icon.TrayIcon()

        self.paused = self.prefs['timer']['paused']

        frame = MainFrame.mainFrame()
        self.SetTopWindow(frame)
        # Frame created, but not shown, at startup


        # Start the timer
        if not self.paused:
            self.syncTimer = wx.CallLater(self.prefs['timer']['pollTimer'] * 1000, self.Sync)
        else:
            self.syncTimer = None

        wx.CallAfter(self.reportInit)

        prefObject = appConfig.wmPrefs()
        return True

    def loadConfig(self):
        """Load config file from disk"""

        try:
            with open(cfg.configFileName) as fp:
                prefs = json.load(fp)
        except FileNotFoundError as e:
            prefs = defaultPrefs.default()

        return prefs

    def saveConfig(self):
        """Save config back to disk"""
        with open(cfg.configFileName, 'w') as fp:
            json.dump(self.prefs, fp)

    def reportInit(self):
        """Dummy to log initialization once the event loop starts to run."""
        wx.FindWindowByName("loggr").critical(f"{version.name} {version.version} Starting up!")

    def CloseApp(self, event):
        """Kill off the program. Hasta la vista, bye bye ..."""
        wx.FindWindowByName("loggr").critical("Shutting down!")

        self.saveConfig()
        self.Tray.Destroy()

        # Due to the funky way that things interact here, we have to kill the app off in
        # little bits. ExitMainLoop stops processing of the app, then we have to 'bump'
        # the event processor to get it to close all open windows.
        self.ExitMainLoop()
        wx.WakeUpIdle()

    def openMainWindow(self):
        """Shows main window (normally hidden)"""
        frame = wx.FindWindowByName("sniprTopFrame")
        frame.Show(True)

    def Sync(self):
        """Checks for changes and executes sync operations when called."""
        wx.FindWindowByName("loggr").debug("Waking up for sync.")

        # todo: sync now from main window
        # todo: sync now from tray icon

        # Start the timer again
        if not self.paused:
            self.syncTimer = wx.CallLater(self.prefs['timer']['pollTimer'] * 1000, self.Sync)
        else:
            self.syncTimer = None

    def togglePause(self, evt):
        """Turn pause on and off."""
        loggr = wx.FindWindowByName("loggr")

        if self.paused:
            # Turn on syncing, but don't start just yet
            loggr.info("Un-pausing Sync.")
            self.paused = False
        else:
            # Turn off syncing, and kill the timer
            loggr.info("Pausing Sync.")
            self.paused = True
            self.syncTimer.Stop()
            self.syncTimer = None

        self.prefs['timer']['paused'] = self.paused
        wx.FindWindowByName("pauseBox").SetValue(self.paused)

        # Actually re-start syncing by calling Sync timer
        if not self.paused:
            self.Sync()


if __name__ == "__main__":
    app = mongerApp()
    app.MainLoop()
    sys.exit(0)


# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79
