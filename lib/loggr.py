# !/usr/bin/python3
#
# loggr - Created 8/26/2017 12:03 PM
#
# Description goes here.
#
# (c) 2001 - 2016 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################

import logging
import logging.config

from . import version

logConfig = {
    'version' : 1,
    "disable_existing_loggers": True,

    'loggers': {
        'snipr': {
            'level': logging.DEBUG,
            "handlers": ['console', 'logfile'],
            'propagate': True,
            'qualname': version.name
        }
    },

    'handlers': {
        'console' : {
            "class": 'logging.StreamHandler',
            "formatter": "brief",
        },
        'logfile': {
            "class"    : 'logging.FileHandler',
            "filename" : "snipr.log",
            "formatter": "default",
            "mode"     : "w"
        }
    },

    'formatters': {
        'default':
            {
                'format': "%(asctime)s %(levelname)7.7s> %(message)s (%(module)s:%(lineno)d)",
                'datefmt': "%Y-%m-%d %H:%M:%S",
            },
        'brief':
            {
                'format' : "%(asctime)s %(levelname)1.1s> %(message)s",
                'datefmt': "%Y-%m-%d %H:%M:%S",
            },
    }
}

logName = "snipr"


def init():
    """Start the log anew."""
    logging.config.dictConfig(logConfig)
    log = logging.getLogger(logName)
    return log


def getLog():
    """Get pre-existing log"""
    log = logging.getLogger("snipr")
    return log

# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79
