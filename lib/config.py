# !/usr/bin/python3
#
# config - Created 2/24/2018
#
# (c) 2018 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################

"""Configuration constants for the application"""

configFileName = 'wowmonger.json'

# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79
