# !/usr/bin/python3
#
# version - Created 2/24/2018
#
# Version info.
#
# (c) 2018 @ Grimmlabs
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################

major       = 1
minor       = 0
rev         = 0

copyDate    = "2018"
name        = "WoWMonger!"
vendor      = "Grimmlabs"

version     = f'{major}.{minor:02}.{rev:02}'
fullVersion = f'{name} {version} (c) {copyDate} by {vendor}'

# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79
