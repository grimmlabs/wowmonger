# !/usr/bin/python3
#
# logger - Created 1/19/2018 6:31 PM
#
# Logging window used by app to capture event data.
#
# (c) 2017 - 2018 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################

import datetime

import logging

import wx


class LoggerPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, name="loggr")

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        self.logWindow = wx.TextCtrl(self, -1, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL | wx.TE_RICH)
        # make it mono font
        fn = self.logWindow.GetFont()
        fn.SetFamily(wx.FONTFAMILY_TELETYPE)
        self.logWindow.SetFont(fn)

        sizer.Add(self.logWindow, proportion=1, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)

        self.SetAutoLayout(True)
        self.SetSizer(sizer)
        sizer.Fit(self)

    def log(self, lvl, textToLog):
        """Logs an event to the log window and native python logger."""

        # timestamp
        now = datetime.datetime.now()
        ts = now.strftime("%Y-%m-%d %H:%M:%S")

        self.logWindow.write(f"{ts} > {textToLog}\n")
        # todo: optionally log to a file.
        # todo: optionally log to stdout

    def debug(self, textToLog):
        self.log(logging.DEBUG, textToLog)

    def info(self, textToLog):
        self.log(logging.INFO, textToLog)

    def critical(self, textToLog):
        self.log(logging.CRITICAL, textToLog)

# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79
