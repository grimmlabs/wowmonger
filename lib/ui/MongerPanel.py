# !/usr/bin/python3
#
# MongerPanel - Created 3/15/2018 11:01 PM
#
# Description goes here.
#
# (c) 2001 - 2018 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx

from . import primitives
from . import logger
from . import wxLED


class dummy(wx.Panel):
    def __init__(self, parent, title):
        wx.Panel.__init__(self, parent)

        sizer = wx.StaticBoxSizer(wx.VERTICAL, self, title)
        self.SetAutoLayout(True)
        self.SetSizer(sizer)
        sizer.Fit(self)


class statusPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        l1 = primitives.label(self, "Sync Enabled")
        l2 = wxLED.LEDCtrl(self, colorStyle=1, size=wx.Size(16, 16))
        l3 = primitives.label(self, "Send")
        l4 = wxLED.LEDCtrl(self, colorStyle=1, size=wx.Size(16, 16))
        l5 = primitives.label(self, "Sync Off")
        l6 = wxLED.LEDCtrl(self, colorStyle=0, size=wx.Size(16, 16))
        l7 = primitives.label(self, "Receive")
        l8 = wxLED.LEDCtrl(self, colorStyle=0, size=wx.Size(16, 16))

        sizer = wx.StaticBoxSizer(wx.HORIZONTAL, self, "Status")

        flex = wx.FlexGridSizer(2, 4, 0, 0)
        flex.AddGrowableCol(0, 1)
        flex.AddGrowableCol(2, 1)

        flex.Add(l1, proportion=1, border=0, flag=wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL)
        flex.Add(l2, proportion=0, border=2, flag=wx.ALL | wx.ALIGN_CENTER)
        flex.Add(l3, proportion=1, border=0, flag=wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL)
        flex.Add(l4, proportion=0, border=2, flag=wx.ALL | wx.ALIGN_CENTER)

        flex.Add(l5, proportion=1, border=0, flag=wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL)
        flex.Add(l6, proportion=0, border=2, flag=wx.ALL | wx.ALIGN_CENTER)
        flex.Add(l7, proportion=1, border=0, flag=wx.ALL | wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL)
        flex.Add(l8, proportion=0, border=2, flag=wx.ALL | wx.ALIGN_CENTER)

        sizer.Add(flex, proportion=1, border=0, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)
        self.SetAutoLayout(True)
        self.SetSizer(sizer)
        sizer.Fit(self)


class buttonPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        self.pauseBox = wx.CheckBox(self, -1, "Paused", name="pauseBox")
        sb2 = wx.Button(self, -1, "Sync Now")
        cb = wx.Button(self, -1, "Close Win")
        qb = wx.Button(self, -1, "Quit")
        ab = wx.Button(self, -1, "About")

        sizer = wx.StaticBoxSizer(wx.VERTICAL, self, "Buttons")

        sizer.Add(self.pauseBox, border=3, flag=wx.ALL | wx.ALIGN_CENTER)
        sizer.Add(sb2, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)
        sizer.Add(ab, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)
        sizer.AddStretchSpacer(prop=1)
        sizer.Add(cb, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)
        sizer.Add(qb, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)

        self.SetAutoLayout(True)
        self.SetSizer(sizer)
        sizer.Fit(self)

        self.Bind(wx.EVT_BUTTON, self.Quit, source=qb)

        self.pauseBox.SetValue(wx.GetApp().prefs['timer']['paused'])
        self.Bind(wx.EVT_CHECKBOX, self.togglePause)

    def Quit(self, evt):
        wx.GetApp().CloseApp(evt)

    def togglePause(self, evt):
        """The pause checkbox was toggled"""
        wx.GetApp().togglePause(evt)


class mainPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        wp = statusPanel(self)
        lp = logger.LoggerPanel(self)

        sizer = wx.StaticBoxSizer(wx.VERTICAL, self, "mainPanel")
        sizer.Add(wp, proportion=0, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)
        sizer.Add(lp, proportion=1, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)

        self.SetAutoLayout(True)
        self.SetSizer(sizer)
        sizer.Fit(self)


class topPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        sizer = wx.BoxSizer(wx.HORIZONTAL)

        mp = mainPanel(self)
        bp = buttonPanel(self)

        sizer.Add(mp, proportion=1, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)
        sizer.Add(bp, proportion=0, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)

        self.SetAutoLayout(True)
        self.SetSizer(sizer)
        sizer.Fit(self)
