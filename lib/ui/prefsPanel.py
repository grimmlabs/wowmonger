# !/usr/bin/python3
#
# prefsPanel - Created 3/2/2018 11:31 PM
#
# Prefs settings.
#
# (c) 2018 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79

import wx
import wx.lib.filebrowsebutton as fb

from .primitives import label


class pathPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.StaticBoxSizer(wx.VERTICAL, self, "Paths")

        addonPath = fb.DirBrowseButton(self, labelText='Location of WoW addons: ')#, changeCallback = self.dbbCallback)
        wftPath = fb.DirBrowseButton(self, labelText='Location of WoW Settings: ')#, changeCallback = self.dbbCallback)

        sizer.Add(addonPath, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)
        sizer.Add(wftPath, proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        self.SetSizer(sizer)
        sizer.Fit(self)


class repoUserPWPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        sizer.Add(label(self, lebbel="Password"), proportion=0, border=3, flag=wx.ALL | wx.ALIGN_CENTRE_VERTICAL)
        repoPW=wx.TextCtrl(self, -1)
        sizer.Add(repoPW, proportion=1, border=3, flag=wx.ALL | wx.ALIGN_CENTRE_VERTICAL)

        self.SetSizer(sizer)
        sizer.Fit(self)


class repoUserNamePanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        sizer.Add(label(self, lebbel="Username"), proportion=0, border=3, flag=wx.ALL | wx.ALIGN_CENTRE_VERTICAL)
        repoUser=wx.TextCtrl(self, -1)
        sizer.Add(repoUser, proportion=1, border=3, flag=wx.ALL | wx.ALIGN_CENTRE_VERTICAL)

        self.SetSizer(sizer)
        sizer.Fit(self)


class repoUserPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.StaticBoxSizer(wx.HORIZONTAL, self, "Repo Account")

        sizer.Add(repoUserNamePanel(self), proportion=1, border=3, flag=wx.ALL | wx.EXPAND)
        sizer.Add(repoUserPWPanel(self), proportion=1, border=3, flag=wx.ALL | wx.EXPAND)

        self.SetSizer(sizer)
        sizer.Fit(self)


class repoProjectPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        sizer.Add(label(self, lebbel="Repo Name"), proportion=0, border=3, flag=wx.ALL | wx.ALIGN_CENTRE_VERTICAL)
        repoName=wx.TextCtrl(self, -1)
        sizer.Add(repoName, proportion=1, border=3, flag=wx.ALL | wx.ALIGN_CENTRE_VERTICAL)

        self.SetSizer(sizer)
        sizer.Fit(self)


class repoPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.BoxSizer(wx.VERTICAL)

        sizer.Add(repoProjectPanel(self), proportion=0, border=3, flag=wx.ALL | wx.EXPAND)
        sizer.Add(repoUserPanel(self), proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        self.SetSizer(sizer)
        sizer.Fit(self)


class serviceChoice(wx.ComboBox):
    def __init__(self, parent):
        wx.ComboBox.__init__(self, parent)


class serviceChoicePanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.StaticBoxSizer(wx.HORIZONTAL, self, "Service Choice")

        sizer.Add(label(self, lebbel="Service"), proportion=0, border=3, flag=wx.ALL | wx.EXPAND)
        sizer.Add(serviceChoice(self), proportion=1, border=3, flag=wx.ALL | wx.EXPAND)

        self.SetSizer(sizer)
        sizer.Fit(self)


class dataPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        sizer = wx.StaticBoxSizer(wx.VERTICAL, self, "Prefs")

        sizer.Add(serviceChoicePanel(self), proportion=0, border=3, flag=wx.ALL | wx.EXPAND)
        sizer.Add(repoPanel(self), proportion=0, border=3, flag=wx.ALL | wx.EXPAND)
        sizer.Add(pathPanel(self), proportion=0, border=3, flag=wx.ALL | wx.EXPAND)

        self.SetSizer(sizer)
        sizer.Fit(self)


class buttonPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        sizer = wx.StaticBoxSizer(wx.VERTICAL, self, "button")

        qb = wx.Button(self, -1, "Quit")

        sizer.AddStretchSpacer(prop=1)
        sizer.Add(qb, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)

        self.SetAutoLayout(True)
        self.SetSizer(sizer)
        sizer.Fit(self)

        self.Bind(wx.EVT_BUTTON, self.Quit, source=qb)

    def Quit(self, evt):
        wx.GetApp().CloseApp(evt)


class prefsPane(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(dataPanel(self), proportion=1, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)
        sizer.Add(buttonPanel(self), proportion=0, border=3, flag=wx.ALL | wx.EXPAND | wx.ALIGN_CENTER)
        self.SetSizer(sizer)
        sizer.Fit(self)


