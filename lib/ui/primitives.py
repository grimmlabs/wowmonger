# !/usr/bin/python3
#
# primitives - Created 2/25/2018 12:38 PM
#
# Description goes here.
#
# (c) 2001 - 2018 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################

"""Primitives used in by other elements of the UI"""

import wx


class label(wx.StaticText):
    def __init__(self, parent, lebbel):
        wx.StaticText.__init__(self, parent, label=f"{lebbel}: ")

        # Make the font bold
        fn = self.GetFont()
        fn.SetWeight(wx.BOLD)
        self.SetFont(fn)


class Indicator(wx.Button):
    """Captain Placeholder ... until I get something more appropraite worked up."""
    def __init__(self, parent):
        wx.Button.__init__(self, parent, label="X", style=wx.BU_EXACTFIT)


# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79
