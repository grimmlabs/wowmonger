# !/usr/bin/python3
#
# appConfig - Created 4/5/2018 6:53 PM
#
# Stores application configuration data in a wx-friendly manner.
#
# (c) 2018 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################
# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79


class gitHost(object):
    gitServers = {
        'bitbucket': {  'baseURL'   : "https://bitbucket.org",
                        'restCreate': "",
        },
    }

    def __init__(self, host):
        hostChoice = self.gitServers[host]

        # base URL that we will use to access the git host Rest API
        self.baseURL    = hostChoice['baseURL']
        # API endpoint for repo creation
        self.restCreate = hostChoice['restCreate']


class appConfig(object):
    def __init__(self):
        object.__init__(self)

        # These are all default settings that will be overwritten by loading from disk.

        #
        # window geometry
        #
        # position
        self.geoPos = (-1, -1)
        # size
        self.geoSize = (-1, -1)

        # server choice indicates which set of settings (api, etc) to choose from in the _gitHosts object above
        self.serverChoice = 'bitbucket'

        self.repoUser = ""
        self.repoPW = ""
        self.repoBranch = "master"

        self.timerPollInterval = 5
        self.timerPaused = False
