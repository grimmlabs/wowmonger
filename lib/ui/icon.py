# !/usr/bin/python3
#
# icon - Created 1/8/2018 6:15 PM
#
# Description goes here.
#
# (c) 2001 - 2016 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################

import wx.adv

from .. import loggr

log = loggr.getLog()

WMMenu_Config   = wx.NewId()
WMMenu_Open     = wx.NewId()
WMMenu_Pause    = wx.NewId()
WMMenu_Quit     = wx.NewId()
WMMenu_Sync     = wx.NewId()


def IconMenu():

    paused = wx.GetApp().paused

    menu = wx.Menu()

    menu.Append(WMMenu_Open, "Open WoWMonger!")
    menu.Append(WMMenu_Config, "Configure WoWMonger!")

    menu.AppendSeparator()

    if paused:
        menu.Append(WMMenu_Pause, "Resume Sync")
    else:
        menu.Append(WMMenu_Pause, "Pause Sync")

    menu.Append(WMMenu_Sync, "Sync Now")

    menu.AppendSeparator()

    menu.Append(WMMenu_Quit, "Quit WoWMonger!")

    return menu


class TrayIcon(wx.adv.TaskBarIcon):
    def __init__(self):
        wx.adv.TaskBarIcon.__init__(self)

        icon = wx.Icon("assets/images/icon15x15.png", type=wx.BITMAP_TYPE_PNG)

        if self.IsOk()	:
            self.SetIcon(icon=icon)

        self.Bind(wx.EVT_MENU, self.sortingHat)
        self.Bind(wx.adv.EVT_TASKBAR_LEFT_DCLICK, self.DoubleClick)

    def CreatePopupMenu(self):
        menu = IconMenu()
        return menu

    def sortingHat(self, evt):
        if evt.Id == WMMenu_Quit:
            wx.GetApp().CloseApp(evt)
        elif evt.Id == WMMenu_Open:
            wx.GetApp().openMainWindow()
        elif evt.Id == WMMenu_Pause:
            wx.GetApp().togglePause(evt)
        else:
            log.debug(f"Unhandled Menu event: [{evt.Id}]")

    def DoubleClick(self, evt):
        wx.GetApp().openMainWindow()

# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79
