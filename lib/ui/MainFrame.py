# !/usr/bin/python3
#
# MainFrame - Created 1/12/2018 5:53 PM
#
# Main UI frame for Snipr
#
# (c) 2001 - 2018 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################

import wx

from .. import version

from .MongerPanel import topPanel
from .prefsPanel import prefsPane


class mainFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, title=version.fullVersion, name="sniprTopFrame")
        self.Bind(wx.EVT_CLOSE, self.onClose)

        nb = wx.Notebook(self, style=wx.NB_TOP | wx.NB_FIXEDWIDTH)
        nb.InsertPage(0, topPanel(nb), "WOWMONGER!", select=True)
        nb.InsertPage(1, prefsPane(nb), "Settings", select=False)

        prefs = wx.GetApp().prefs
        self.SetPosition(prefs['geo']['pos'])
        self.SetSize(prefs['geo']['size'])

    def onClose(self, evt):
        # wx.CallAfter(self.FindWindowByName("loggr").debug, "Hiding mainFrame.")
        pos = self.GetPosition()
        size = self.GetSize()

        prefs = wx.GetApp().prefs
        prefs['geo']['pos'] = [pos.x, pos.y]
        prefs['geo']['size'] = [size.x, size.y]

        self.Show(False)


# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79
