#! /c/Python/python
#----------------------------------------------------------------------
# Name:         wxLED
# Purpose:      A class that simulates an LED indicator
#
# Author:       Jeff Grimmett (grimmtooth@softhome.net)
#
# Copyright:    (c) 2018 by GrimmLabs
# Licence:      wxWindows license
#----------------------------------------------------------------------

import wx


class LEDCtrl(wx.Control):
    def __init__(self, parent, size=wx.Size(25, 25), colorStyle=0):

        self.state = False
        self.colorStyle = colorStyle

        if self.colorStyle == 0:
            # red
            self.Lit = wx.RED
            self.Dark = wx.Colour(145, 0, 0)
        elif self.colorStyle == 1:
            # green
            self.Lit = wx.Colour(0, 255, 0)
            self.Dark = wx.GREEN

        wx.Control.__init__(self, parent, size=size, style=wx.BORDER_NONE)
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        
    def OnPaint(self, evt):
        dc = wx.PaintDC(self)
        
        w, h = self.GetClientSize()

        if self.state:
            dc.SetBrush(wx.Brush(self.Lit))
        else:
            dc.SetBrush(wx.Brush(self.Dark))

        radius = min(w, h) / 2
        center = (w / 2, h / 2)
        dc.DrawCircle(center, radius)

        evt.Skip()

    def SetState(self, state):
        if state == True:
            self.state = True
        else:
            self.state = False
    
        self.Refresh()

    def ToggleState(self):
        if self.state == True:
            self.SetState(False)
        else:
            self.SetState(True)

