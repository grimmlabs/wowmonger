# !/usr/bin/python3
#
# defaultPrefs - Created 2/21/2018 5:34 PM
#
# Description goes here.
#
# (c) 2001 - 2018 @ Grimmlabs
#
# See license file for redistribution details. 
#
# Author: Jeff Grimmett (grimmtooth@gmail.com)
###########################################################################

"""Define default preferences for first-run."""

_gitServers = {
    'bitbucket':{
        'baseURL':"https://bitbucket.org",
        'restCreate':"",
    }
}

_defaults = {
    'geo':  {
        'pos': (-1, -1),    # window position
        'size': (-1, -1),   # window size
    },
    'server':'bitbucket',
    'repo': {
        "username":"",
        "pw":"",
        "branch":"master",
    },
    'timer': {
        'pollTimer': 5,
        'paused': False,
    },
}


def default():
    return _defaults

# vim: shiftwidth=4 tabstop=4 softtabstop=4 expandtab textwidth=79
